#trying to convert images into grayscale as it requires less memory utilization

#thought of labelling out the blurred and clear image in set of two classes for training purpose
#tried to encode blurred images as 0 and non-blurred images as 1
#after identifying image as blurred or not
#model was not checked from myside as its a approach for solving out the given problem
#requires more paper work to clean out the model building.
#thought for going with GRNN as it will be a flexible approach but hopefully this approach will work
#need time to study out some research papers for implementing running model.
#bnjan_raj@yahoo.co.in


from imutils import paths
import argparse
import cv2
import tensorflow as tf
import numpy as np
import os
from random import shuffle
from tqdm import tqdm
import tensorflow as tf
import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression

TRAIN_DIR = 'train'
TEST_DIR = 'test1'
IMG_SIZE = 50
LR = 1e -3
MODEL_NAME = 'blurred-Vs-non-blurred'


#importing images

def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images", required=True,
	help="path to input directory of images")
ap.add_argument("-t", "--threshold", type=float, default=100.0,
	help="focus measures that fall below this value will be considered 'blurry'")
args = vars(ap.parse_args())

# loop over the input images

for imagePath in paths.list_images(args["images"]):
	# load the image, convert it to grayscale, and compute the
	# focus measure of the image using the Variance of Laplacian
	# method
	image = cv2.imread(imagePath)
    height, width = image.shape[:2]
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	fm = variance_of_laplacian(gray)

    if fm < args["threshold"]:
        text = "blurred"
        return np.array([0,1])
    else:
        return np.array([1,0])


def create_train _data():
    training_data = []


    for img in tqdm(os.listdir(TRAIN_DIR)):
        path = os.path.join(TRAIN_DIR,img)
        img_data = cv2.imread(path,cv2.IMREAD_GRAYSCALE )
        img_data = cv2.resize(img_data, (IMG_SIZE,IMG_SIZE))
        training_data.append([np.array(img_data),variance_of_laplacian(img)])

        shuffle(training_data)
        np.save('training_data.npy',training_data)
        return training_data

def create_test_data():
    testing_data = []

    for img in tqdm(os.listdir(TEST_DIR)):
        path = os.path.join(TEST_DIR,img)
        img_data = cv2.imread(path,cv2.IMREAD_GRAYSCALE )
        img_data = cv2.resize(img_data, (IMG_SIZE,IMG_SIZE))
        testing_data.append([np.array(img_data),img_num])

        shuffle(training_data)
        np.save('testing_data.npy',testing_data)
        return testing_data


train_data = create_test_data()
test_data = create_test_data()


train = train_data[:-500]
test = test_data[-500:]

X_train = np.array([i[0] for i in train]).reshape(-1,IMG_SIZE,IMG_SIZE,1)
Y_train = np.array(i[1] for i in train)
X_test = np.array([i[0] for i in test]).reshape(-1,IMG_SIZE,IMG_SIZE,1)

Y_test = np.array(i[1] for i in test)


#network architecture

tf.reset_default_graph()

convnet = input_data(shape=[None,IMG_SIZE,IMG_SIZE,1],name='input')
convnet = conv_2d(convnet,32,5,activation='relu')
convnet = max_pool_2d(convnet,5)
convnet = conv_2d(convnet,64,5,activation='relu')
convnet = max_pool_2d(convnet,5)
convnet = conv_2d(convnet,128,5,activation='relu')
convnet = conv_2d(convnet,64,5,activation='relu')
convnet = max_pool_2d(convnet,5)
convnet = conv_2d(convnet,32,5,activation='relu')
convnet = max_pool_2d(convnet,5)
convnet = fully_connected(convnet,1024,activation='relu')
convnet = dropout(convnet,0.8)
convnet = fully_connected(convnet,2,activation='softmax')
convnet = regression(convnet,optimizer='adam',learning_rate = LR, loss = 'categorical_crossentropy')
model.fit({'input':X_train},{'targets':Y_train},n_epoch = 10,validation_set = ({'input':X_test},{'targets':Y_test}),snapshot_step = 500,show_metric= True,run_id=Model_name)

