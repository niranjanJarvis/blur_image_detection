# blur_image_detection

It was an approach to classify out blurred images with sharp ones.
i was trying to classify blurred and sharp ones in two different sets i.e i tried to provide label to blurred images as 0 and sharp ones as 1,
converted images into grayscale, did hot encoding for classification.
i was trying to model out the problem as a binary classification problem and i am sure it can be model out as binomial approach problem.
model consist of sets of conv nets , pooling layer arranged alternatively and finally a fully connected layer where activation used is softmax function.
although i did not run the model as more paper work was needed but i am presenting you a problem solving technique, paying less attention to the coding habits.
